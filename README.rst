.. image:: 
   _static/Logo.png

SSH Server
*********************************************************************

.. contents:: Content
    :depth: 3

The following chapter will describe measures to increase the security for SSH Server.

Original
---------------------------------------------------------------------

.. Code:: Bash

    #	$OpenBSD: sshd_config,v 1.103 2018/04/09 20:41:22 tj Exp $

    # This is the sshd server system-wide configuration file.  See
    # sshd_config(5) for more information.

    # This sshd was compiled with PATH=/usr/bin:/bin:/usr/sbin:/sbin

    # The strategy used for options in the default sshd_config shipped with
    # OpenSSH is to specify options with their default value where
    # possible, but leave them commented.  Uncommented options override the
    # default value.

    #Port 22
    #AddressFamily any
    #ListenAddress 0.0.0.0
    #ListenAddress ::

    #HostKey /etc/ssh/ssh_host_rsa_key
    #HostKey /etc/ssh/ssh_host_ecdsa_key
    #HostKey /etc/ssh/ssh_host_ed25519_key

    # Ciphers and keying
    #RekeyLimit default none

    # Logging
    #SyslogFacility AUTH
    #LogLevel INFO

    # Authentication:

    #LoginGraceTime 2m
    #PermitRootLogin prohibit-password
    #StrictModes yes
    #MaxAuthTries 6
    #MaxSessions 10

    #PubkeyAuthentication yes

    # The default is to check both .ssh/authorized_keys and .ssh/authorized_keys2
    # but this is overridden so installations will only check .ssh/authorized_keys
    AuthorizedKeysFile	.ssh/authorized_keys

    #AuthorizedPrincipalsFile none

    #AuthorizedKeysCommand none
    #AuthorizedKeysCommandUser nobody

    # For this to work you will also need host keys in /etc/ssh/ssh_known_hosts
    #HostbasedAuthentication no
    # Change to yes if you don't trust ~/.ssh/known_hosts for
    # HostbasedAuthentication
    #IgnoreUserKnownHosts no
    # Don't read the user's ~/.rhosts and ~/.shosts files
    #IgnoreRhosts yes

    # To disable tunneled clear text passwords, change to no here!
    #PasswordAuthentication yes
    #PermitEmptyPasswords no

    # Change to no to disable s/key passwords
    #ChallengeResponseAuthentication yes

    # Kerberos options
    #KerberosAuthentication no
    #KerberosOrLocalPasswd yes
    #KerberosTicketCleanup yes
    #KerberosGetAFSToken no

    # GSSAPI options
    #GSSAPIAuthentication no
    #GSSAPICleanupCredentials yes

    # Set this to 'yes' to enable PAM authentication, account processing,
    # and session processing. If this is enabled, PAM authentication will
    # be allowed through the ChallengeResponseAuthentication and
    # PasswordAuthentication.  Depending on your PAM configuration,
    # PAM authentication via ChallengeResponseAuthentication may bypass
    # the setting of "PermitRootLogin without-password".
    # If you just want the PAM account and session checks to run without
    # PAM authentication, then enable this but set PasswordAuthentication
    # and ChallengeResponseAuthentication to 'no'.
    UsePAM yes

    #AllowAgentForwarding yes
    #AllowTcpForwarding yes
    #GatewayPorts no
    #X11Forwarding no
    #X11DisplayOffset 10
    #X11UseLocalhost yes
    #PermitTTY yes
    #PrintMotd yes
    #PrintLastLog yes
    #TCPKeepAlive yes
    #PermitUserEnvironment no
    #Compression delayed
    #ClientAliveInterval 0
    #ClientAliveCountMax 3
    #UseDNS no
    #PidFile /var/run/sshd.pid
    #MaxStartups 10:30:100
    #PermitTunnel no
    #ChrootDirectory none
    #VersionAddendum none

    # pass locale information
    AcceptEnv LANG LC_*

    # no default banner path
    #Banner none

    # override default of no subsystems
    Subsystem	sftp	/usr/libexec/sftp-server

    # Example of overriding settings on a per-user basis
    #Match User anoncvs
    #	X11Forwarding no
    #	AllowTcpForwarding no
    #	PermitTTY no
    #	ForceCommand cvs server

    # XAuthLocation added by XQuartz (https://www.xquartz.org)
    XAuthLocation /opt/X11/bin/xauth

Modify
---------------------------------------------------------------------

change to the following:

.. Code:: Bash

    #	$OpenBSD: sshd_config,v 1.103 2018/04/09 20:41:22 tj Exp $

    # This is the sshd server system-wide configuration file.  See
    # sshd_config(5) for more information.

    # This sshd was compiled with PATH=/usr/bin:/bin:/usr/sbin:/sbin

    # The strategy used for options in the default sshd_config shipped with
    # OpenSSH is to specify options with their default value where
    # possible, but leave them commented.  Uncommented options override the
    # default value.

    #Port 22
    #AddressFamily any
    #ListenAddress 0.0.0.0
    #ListenAddress ::

    #HostKey /etc/ssh/ssh_host_rsa_key
    #HostKey /etc/ssh/ssh_host_ecdsa_key
    #HostKey /etc/ssh/ssh_host_ed25519_key

    # Ciphers and keying
    #RekeyLimit default none

    # Logging
    #SyslogFacility AUTH
    #LogLevel INFO

    # Authentication:

    #LoginGraceTime 2m
    #PermitRootLogin prohibit-password
    #StrictModes yes
    #MaxAuthTries 6
    #MaxSessions 10

    #PubkeyAuthentication yes

    # Expect .ssh/authorized_keys2 to be disregarded by default in future.
    #AuthorizedKeysFile	.ssh/authorized_keys .ssh/authorized_keys2

    #AuthorizedPrincipalsFile none

    #AuthorizedKeysCommand none
    #AuthorizedKeysCommandUser nobody

    # For this to work you will also need host keys in /etc/ssh/ssh_known_hosts
    #HostbasedAuthentication no
    # Change to yes if you don't trust ~/.ssh/known_hosts for
    # HostbasedAuthentication
    #IgnoreUserKnownHosts no
    # Don't read the user's ~/.rhosts and ~/.shosts files
    #IgnoreRhosts yes

    # To disable tunneled clear text passwords, change to no here!
    #PasswordAuthentication yes
    #PermitEmptyPasswords no

    # Change to yes to enable challenge-response passwords (beware issues with
    # some PAM modules and threads)
    ChallengeResponseAuthentication no

    # Kerberos options
    #KerberosAuthentication no
    #KerberosOrLocalPasswd yes
    #KerberosTicketCleanup yes
    #KerberosGetAFSToken no

    # GSSAPI options
    #GSSAPIAuthentication no
    #GSSAPICleanupCredentials yes
    #GSSAPIStrictAcceptorCheck yes
    #GSSAPIKeyExchange no

    # Set this to 'yes' to enable PAM authentication, account processing,
    # and session processing. If this is enabled, PAM authentication will
    # be allowed through the ChallengeResponseAuthentication and
    # PasswordAuthentication.  Depending on your PAM configuration,
    # PAM authentication via ChallengeResponseAuthentication may bypass
    # the setting of "PermitRootLogin without-password".
    # If you just want the PAM account and session checks to run without
    # PAM authentication, then enable this but set PasswordAuthentication
    # and ChallengeResponseAuthentication to 'no'.
    UsePAM yes

    #AllowAgentForwarding yes
    #AllowTcpForwarding yes
    #GatewayPorts no
    X11Forwarding yes
    #X11DisplayOffset 10
    #X11UseLocalhost yes
    #PermitTTY yes
    PrintMotd no
    #PrintLastLog yes
    #TCPKeepAlive yes
    #PermitUserEnvironment no
    Compression no
    #Compression disabled
    #ClientAliveInterval 0
    #ClientAliveCountMax 3
    #UseDNS no
    #PidFile /var/run/sshd.pid
    #MaxStartups 10:30:100
    #PermitTunnel no
    #ChrootDirectory none
    #VersionAddendum none

    # no default banner path
    #Banner none

    # Allow client to pass locale environment variables
    AcceptEnv LANG LC_*

    # override default of no subsystems
    Subsystem	sftp	/usr/lib/openssh/sftp-server

    # Example of overriding settings on a per-user basis
    #Match User anoncvs
    #	X11Forwarding no
    #	AllowTcpForwarding no
    #	PermitTTY no
    #	ForceCommand cvs server

    # Restrict key exchange, cipher, and MAC algorithms, as per VCS
    # hardening guide.
    KexAlgorithms curve25519-sha256@libssh.org
    Ciphers chacha20-poly1305@openssh.com,aes256-gcm@openssh.com 
    MACs hmac-sha2-512-etm@openssh.com,umac-128-etm@openssh.com

**the most important lines are:**

.. Code:: Bash

    # Restrict key exchange, cipher, and MAC algorithms, as per VCS
    # hardening guide.
    KexAlgorithms curve25519-sha256@libssh.org
    Ciphers chacha20-poly1305@openssh.com,aes256-gcm@openssh.com 
    MACs hmac-sha2-512-etm@openssh.com,umac-128-etm@openssh.com

.. Notes:: Restart you sshd daemon

Control
---------------------------------------------------------------------

check with ssh-audit [1]_ your /etc/sshd_config:

ssh-audit SHOULD be installed any way but on macOS use brew install *ssh-audit*.

The result SHOULD be like this on your macOS and each future Linux box:

.. Code:: Bash

    ssh-audit 192.168.1.58
    # general
    (gen) banner: SSH-2.0-OpenSSH_7.9p1 Debian-10+deb10u2
    (gen) software: OpenSSH 7.9p1
    (gen) compatibility: OpenSSH 6.5+, Dropbear SSH 2013.62+
    (gen) compression: disabled

    # key exchange algorithms
    (kex) curve25519-sha256@libssh.org   -- [info] available since OpenSSH 6.5, Dropbear SSH 2013.62

    # host-key algorithms
    (key) ssh-ed25519                    -- [info] available since OpenSSH 6.5

    # encryption algorithms (ciphers)
    (enc) chacha20-poly1305@openssh.com  -- [info] available since OpenSSH 6.5
                                        `- [info] default cipher since OpenSSH 6.9.
    (enc) aes256-gcm@openssh.com         -- [info] available since OpenSSH 6.2

    # message authentication code algorithms
    (mac) hmac-sha2-512-etm@openssh.com  -- [info] available since OpenSSH 6.2
    (mac) umac-128-etm@openssh.com       -- [info] available since OpenSSH 6.2

    # fingerprints
    (fin) ssh-ed25519: SHA256:5f0zt8dNXa+Tye8R/ISlvEiwCNVQv5V5neGMuAWt5z8

    # algorithm recommendations (for OpenSSH 7.9)
    (rec) +aes128-ctr                    -- enc algorithm to append 
    (rec) +aes128-gcm@openssh.com        -- enc algorithm to append 
    (rec) +aes192-ctr                    -- enc algorithm to append 
    (rec) +aes256-ctr                    -- enc algorithm to append 
    (rec) +curve25519-sha256             -- kex algorithm to append 
    (rec) +diffie-hellman-group-exchange-sha256-- kex algorithm to append 
    (rec) +diffie-hellman-group14-sha256 -- kex algorithm to append 
    (rec) +diffie-hellman-group16-sha512 -- kex algorithm to append 
    (rec) +diffie-hellman-group18-sha512 -- kex algorithm to append 
    (rec) +hmac-sha2-256-etm@openssh.com -- mac algorithm to append 
    (rec) +rsa-sha2-256                  -- key algorithm to append 
    (rec) +rsa-sha2-512                  -- key algorithm to append 

Ignore
---------------------------------------------------------------------

Please ignore the suggestions of ssh-audit [1]_ 

    # algorithm recommendations (for OpenSSH 7.9)
    (rec) +aes128-ctr                    -- enc algorithm to append 
    (rec) +aes128-gcm@openssh.com        -- enc algorithm to append 
    (rec) +aes192-ctr                    -- enc algorithm to append 
    (rec) +aes256-ctr                    -- enc algorithm to append 
    (rec) +curve25519-sha256             -- kex algorithm to append 
    (rec) +diffie-hellman-group-exchange-sha256-- kex algorithm to append 
    (rec) +diffie-hellman-group14-sha256 -- kex algorithm to append 
    (rec) +diffie-hellman-group16-sha512 -- kex algorithm to append 
    (rec) +diffie-hellman-group18-sha512 -- kex algorithm to append 
    (rec) +hmac-sha2-256-etm@openssh.com -- mac algorithm to append 
    (rec) +rsa-sha2-256                  -- key algorithm to append 
    (rec) +rsa-sha2-512                  -- key algorithm to append 
    
.....

.. Rubric:: Footnotes

.. [1]
   https://github.com/jtesta/ssh-audit
