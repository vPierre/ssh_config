# http://augustl.com/blog/2009/global_gitignores
# https://github.com/github/gitignore

# Lines starting with `#` are comments.

# Ignore files called 'file.ext'
# file.ext

# Comments can't be on the same line as rules!
# The following line ignores files called 'file.ext # not a comment'
# file.ext # not a comment 

# Ignoring files with full path.
# This matches files in the root directory and subdirectories too.
# i.e. otherfile.ext will be ignored anywhere on the tree.
# dir/otherdir/file.ext
# otherfile.ext

# Ignoring directories
# Both the directory itself and its contents will be ignored.
# bin/
# gen/

# Glob pattern can also be used here to ignore paths with certain characters.
# For example, the below rule will match both build/ and Build/
# [bB]uild/

# Without the trailing slash, the rule will match a file and/or
# a directory, so the following would ignore both a file named `gen`
# and a directory named `gen`, as well as any contents of that directory
# bin
# gen

# Ignoring files by extension
# All files with these extensions will be ignored in
# this directory and all its sub-directories.
# *.apk
# *.class

# It's possible to combine both forms to ignore files with certain
# extensions in certain directories. The following rules would be
# redundant with generic rules defined above.
# java/*.apk
# gen/*.class

# To ignore files only at the top level directory, but not in its
# subdirectories, prefix the rule with a `/`
# /*.apk
# /*.class

# To ignore any directories named DirectoryA 
# in any depth use ** before DirectoryA
# Do not forget the last /, 
# Otherwise it will ignore all files named DirectoryA, rather than directories
# **/DirectoryA/

# This would ignore 
# DirectoryA/
# DirectoryB/DirectoryA/ 
# DirectoryC/DirectoryB/DirectoryA/
# It would not ignore a file named DirectoryA, at any level

# To ignore any directory named DirectoryB within a 
# directory named DirectoryA with any number of 
# directories in between, use ** between the directories
# DirectoryA/**/DirectoryB/

# This would ignore 
# DirectoryA/DirectoryB/ 
# DirectoryA/DirectoryQ/DirectoryB/ 
# DirectoryA/DirectoryQ/DirectoryW/DirectoryB/

# To ignore a set of files, wildcards can be used, as can be seen above.
# A sole '*' will ignore everything in your folder, including your .gitignore file.
# To exclude specific files when using wildcards, negate them.
# So they are excluded from the ignore list:
# !.gitignore 

# Use the backslash as escape character to ignore files with a hash (#)
# (supported since 1.6.2.1)
# \#*#

###################################
# Common
###################################

# ignore all logs *.log

# Packages #
############
# it's better to unpack these files and commit the raw source
# git has its own built in compression methods
#*.7z
*.dmg
#*.gz
*.iso
#*.jar
#*.rar
#*.tar
#*.zip

###################################
# GPG
###################################
# https://github.com/github/gitignore/blob/master/Global/GPG.gitignore

secring.*

###################################
# PuTTY
###################################
# https://github.com/github/gitignore/blob/master/Global/PuTTY.gitignore

# Private key
*.ppk

###################################
# Linux
###################################
# https://github.com/github/gitignore/blob/master/Global/Linux.gitignore

*~

# temporary files which can be created if a process still has a handle open of a deleted file
.fuse_hidden*

# KDE directory preferences
.directory

# Linux trash folder which might appear on any partition or disk
.Trash-*

# .nfs files are created when an open file is removed but is still being accessed
.nfs*

###################################
# OSX
###################################
# General
.DS_Store
.DS_Store?
.AppleDouble
.LSOverride

# Icon must end with two \r
Icon

# Thumbnails
._*
ehthumbs.db
Thumbs.db

# Files that might appear in the root of a volume
.DocumentRevisions-V100
.fseventsd
.Spotlight-V100
.TemporaryItems
.Trashes
.VolumeIcon.icns
.com.apple.timemachine.donotpresent

# Directories potentially created on remote AFP share
.AppleDB
.AppleDesktop
Network Trash Folder
Temporary Items
.apdisk

###################################
# Windows
###################################
# https://github.com/github/gitignore/blob/master/Global/Windows.gitignore

# Windows thumbnail cache files
Thumbs.db
Thumbs.db:encryptable
ehthumbs.db
ehthumbs_vista.db

# Dump file
*.stackdump

# Folder config file
[Dd]esktop.ini

# Recycle Bin used on file shares
$RECYCLE.BIN/

# Windows Installer files
*.cab
*.msi
*.msix
*.msm
*.msp

# Windows shortcuts
*.lnk

###################################
# Microsoft Office
###################################
# https://github.com/github/gitignore/blob/master/Global/MicrosoftOffice.gitignore

*.tmp

# Word temporary
~$*.doc*

# Word Auto Backup File
Backup of *.doc*

# Excel temporary
~$*.xls*

# Excel Backup File
*.xlk

# PowerPoint temporary
~$*.ppt*

# Visio autosave temporary files
*.~vsd*

###################################
# LibreOffice
###################################
# https://github.com/github/gitignore/blob/master/Global/LibreOffice.gitignore

# LibreOffice locks
.~lock.*#

###################################
# Packer
###################################
# https://github.com/github/gitignore

# Cache objects
packer_cache/

# Crash log
crash.log

# For built boxes
*.box

###################################
# Terraform
###################################
# https://github.com/github/gitignore

# Local .terraform directories
**/.terraform/*

# .tfstate files
*.tfstate
*.tfstate.*

# Crash log files
crash.log

# Exclude all .tfvars files, which are likely to contain sentitive data, such as
# password, private keys, and other secrets. These should not be part of version 
# control as they are data points which are potentially sensitive and subject 
# to change depending on the environment.
#
*.tfvars

# Ignore override files as they are usually used to override resources locally and so
# are not checked in
override.tf
override.tf.json
*_override.tf
*_override.tf.json

# Include override files you do wish to add to version control using negated pattern
#
# !example_override.tf

# Include tfplan files to ignore the plan output of command: terraform plan -out=tfplan
# example: *tfplan*

# Ignore CLI configuration files
.terraformrc
terraform.rc

###################################
# Ansible
###################################
# https://github.com/github/gitignore/blob/master/Global/Ansible.gitignore

###################################
# Dropbox
###################################
# https://github.com/github/gitignore/blob/master/Global/Dropbox.gitignore

# Dropbox settings and caches
.dropbox
.dropbox.attr
.dropbox.cache

###################################
# Ansible
###################################
# https://github.com/github/gitignore/blob/master/Global/Emacs.gitignore

# -*- mode: gitignore; -*-
*~
\#*\#
/.emacs.desktop
/.emacs.desktop.lock
*.elc
auto-save-list
tramp
.\#*

# Org-mode
.org-id-locations
*_archive

# flymake-mode
*_flymake.*

# eshell files
/eshell/history
/eshell/lastdir

# elpa packages
/elpa/

# reftex files
*.rel

# AUCTeX auto folder
/auto/

# cask packages
.cask/
dist/

# Flycheck
flycheck_*.el

# server auth directory
/server/

# projectiles files
.projectile

# directory configuration
.dir-locals.el

# network security
/network-security.data

###################################
# Vagrant
###################################
# https://github.com/github/gitignore/blob/master/Global/Vagrant.gitignore

# General
.vagrant/

# Log files (if you are creating logs in debug mode, uncomment this)
*.log

###################################
# Vim
###################################
# https://github.com/github/gitignore/blob/master/Global/Vim.gitignore

# Swap
[._]*.s[a-v][a-z]
!*.svg  # comment out if you don't need vector files
[._]*.sw[a-p]
[._]s[a-rt-v][a-z]
[._]ss[a-gi-z]
[._]sw[a-p]

# Session
Session.vim
Sessionx.vim

# Temporary
.netrwhist
*~
# Auto-generated tag files
tags
# Persistent undo
[._]*.un~

###################################
# Xcode
###################################
# https://github.com/github/gitignore/blob/master/Global/Xcode.gitignore

# Xcode
#
# gitignore contributors: remember to update Global/Xcode.gitignore, Objective-C.gitignore & Swift.gitignore

## User settings
xcuserdata/

## compatibility with Xcode 8 and earlier (ignoring not required starting Xcode 9)
*.xcscmblueprint
*.xccheckout

## compatibility with Xcode 3 and earlier (ignoring not required starting Xcode 4)
build/
DerivedData/
*.moved-aside
*.pbxuser
!default.pbxuser
*.mode1v3
!default.mode1v3
*.mode2v3
!default.mode2v3
*.perspectivev3
!default.perspectivev3

## Gcc Patch
/*.gcno

###################################
# Visual Studio Code
###################################
# https://github.com/github/gitignore/blob/master/Global/VisualStudioCode.gitignore

.vscode/*
!.vscode/settings.json
!.vscode/tasks.json
!.vscode/launch.json
!.vscode/extensions.json
!.vscode/*.code-snippets

# Local History for Visual Studio Code
.history/

# Built Visual Studio Code Extensions
*.vsix

###################################
# Java
###################################
# https://github.com/dclong/xinstall/blob/dev/xinstall/data/git/gitignore_java

# Java 
*.class

## BlueJ files
*.ctxt

## Mobile Tools for Java (J2ME)
.mtj.tmp/

## Package Files
*.jar
*.war
*.ear

# Gradle 
.gradle
/build/
/out/

## Ignore Gradle GUI config
gradle-app.setting

## Avoid ignoring Gradle wrapper jar file (.jar files are usually ignored)
!gradle-wrapper.jar

## Cache of project
.gradletasknamecache

# virtual machine crash logs, see http://www.java.com/en/download/help/error_hotspot.xml
hs_err_pid*

# IDE
.theia/
.idea/
.vscode/
*.ipr
*.iws

# Misc
core
*.log
deprecated

###################################
# Python
###################################
# https://github.com/dclong/xinstall/blob/dev/xinstall/data/git/gitignore_python
.idea/
.theia/
.vscode/
*.ipr
*.iws
.coverage
.mypy/
.mypy_cache/
.pytype/
*.crc
__pycache__/
venv/
.venv/
target/
dist/
*.egg-info/
doc*/_build/
*.prof
core

# gitginore template for creating Snap packages
# website: https://snapcraft.io/

parts/
prime/
stage/
*.snap

# Snapcraft global state tracking data(automatically generated)
# https://forum.snapcraft.io/t/location-to-save-global-state/768
/snap/.snapcraft/

# Source archive packed by `snapcraft cleanbuild` before pushing to the LXD container
/*_source.tar.bz2

# gitignore template for Jupyter Notebooks
# website: http://jupyter.org/

# Remove previous ipynb_checkpoints
#   git rm -r .ipynb_checkpoints/
.ipynb_checkpoints
*/.ipynb_checkpoints/*

# IPython
profile_default/
ipython_config.py


# temporary files which can be created if a process still has a handle open of a deleted file
.fuse_hidden*

# KDE directory preferences
.directory

# Linux trash folder which might appear on any partition or disk
.Trash-*

# .nfs files are created when an open file is removed but is still being accessed
.nfs*

# Windows thumbnail cache files
Thumbs.db
Thumbs.db:encryptable
ehthumbs.db
ehthumbs_vista.db

# Dump file
*.stackdump

# Folder config file
[Dd]esktop.ini

# Recycle Bin used on file shares
$RECYCLE.BIN/

# macOS General
.DS_Store
.AppleDouble
.LSOverride

## Icon must end with two \r
Icon

## Thumbnails
._*

## Files that might appear in the root of a volume
.DocumentRevisions-V100
.fseventsd
.Spotlight-V100
.TemporaryItems
.Trashes
.VolumeIcon.icns
.com.apple.timemachine.donotpresent

## Directories potentially created on remote AFP share
.AppleDB
.AppleDesktop
Network Trash Folder
Temporary Items
.apdisk
