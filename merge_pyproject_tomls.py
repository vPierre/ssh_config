#!/usr/bin/env python3
# coding: utf-8
"""Pyproject.toml Generator.

This script reads the global pyproject config which will be dumped as
``global_pyproject.toml``. It contains the configuration for all the tools that
are used and can be configured via a pyproject.toml file. Then, it reads the
``pyproject.toml`` file located in the repository. In case this is not found,
the global one will be placed as the new pyproject.toml file. Yet if it is
found, then the tool configuration will be updated by the global one, whereas
the rest will be kept as it is.

Author: André Breuer
Created: 2023-03-27
Tested on: macOS Ventura ARM, Debian 11.x x86
"""
__license__ = "Apache-2.0"
__copyright__ = "Copyright (C) 2023 ndaal GmbH & Co KG"

import copy
import os
import sys
from pathlib import Path

import toml


def _main() -> None:
    global_config_file = "global_pyproject.toml"
    local_config_file = "pyproject.toml"
    script_dir = os.path.dirname(__file__)
    global_dir = os.path.join(script_dir, global_config_file)
    local_dir = os.path.join(script_dir, local_config_file)

    if not os.path.exists(global_dir):
        raise FileNotFoundError(
            "No global configuration found. Please provide one."
        )

    if os.path.exists(local_dir):
        print(f"Found repo specific configuration {local_config_file}.")
        final_config = _get_final_config(global_dir, local_dir)
        print("Done merging. Updating old config.")
    else:
        print(
            f"Only global configuration found. Saving as {local_config_file}."
        )
        final_config = _open_toml(global_dir)

    with open(local_config_file, "w", encoding="utf-8") as f:
        toml.dump(final_config, f)

    print("Done.")


def _get_final_config(global_config_path: str, local_config_path: str) -> dict:
    if local_config_path != "" and Path(local_config_path).exists():
        return _merge_config(global_config_path, local_config_path)

    return _open_toml(global_config_path)


def _merge_config(global_config_path: str, local_config_path: str) -> dict:
    global_config = _open_toml(global_config_path)
    local_config = _open_toml(local_config_path)
    merged_config = copy.deepcopy(local_config)

    local_tools = set(local_config.get("tool", {}).keys())
    global_tools = set(global_config.get("tool", {}).keys())

    for key in local_tools & global_tools:
        merged_config.get("tool", {})[key].update(global_config["tool"][key])

    if merged_config.get("tool") is None:
        merged_config["tool"] = {}

    for key in global_tools - local_tools:
        merged_config["tool"][key] = global_config["tool"][key]

    return merged_config


def _open_toml(path: str) -> dict:
    try:
        data = toml.load(path)
    except TypeError:
        print(f"Error opening the {path} file.", file=sys.stderr)
        sys.exit(1)
    except toml.TomlDecodeError:
        print(f"Error decoding the {path} file.", file=sys.stderr)
        sys.exit(1)

    return data


if __name__ == "__main__":
    _main()
